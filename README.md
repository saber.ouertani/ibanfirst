## iBanFirst API Admin

- Description : Data Administration for iBanFirst API : [financialMovements, wallets] Endpoints<br />
- API Documentation : [iBanFirst API Documentation](https://api.ibanfirst.com/APIDocumentation/)<br />

 

- Technology :
This project was developped with React.js / [React Admin](https://marmelab.com/react-admin/Readme.html) Framework.<br />


## Prerequisites

First, you will need Node and npm installed globally on your machine. [ [Install Node.js](https://nodejs.org/en/download/) ].

## Installation and Setup Instructions

    
1. Clone project from GitLab :<br />

    `git clone https://gitlab.com/saber.ouertani/ibanfirst.git`
    
    
2. Go to project folder :<br />

    `cd ibanfirst`

3. Install Dependencies (installing packages in /node_modules folder from package.json file) :<br />

    `npm install`

4. Run the app in the development mode :<br />

    `npm start`

    It will open [http://localhost:3000](http://localhost:3000) to view it in the browser.
    
    The page will reload if you make edits.<br />
    You will also see any lint errors in the console.

5. The login page will be displayed as default : [http://localhost:3000/#/login](http://localhost:3000/#/login), enter your API access (login/Pass) as (username/password)<br />


## Project Structure

    ibanfirst
    │
    ├── README.md
    ├── node_modules
    ├── package.json
    ├── .gitignore
    ├─  public
    │   │
    │   ├── favicon.ico
    │   ├── logo.png
    │   ├── index.html
    │   ├── robots.txt
    │   ├── wsse.js
    │   └── manifest.json
    └── src
        ├── App.css
        ├── App.js
        ├── App.test.js
        ├── index.css
        ├── index.js
        ├── logo.png
        └── DAO
        │   ├─  authProvider.js
        │   ├── iBanFirstDataProvider.js
        │   └── iBanFirstHttpRequest.js
        └── init
        │   └── i18n
        │   │   ├─  en.js
        │   │   ├── fr.js
        │   │   └── index.js 
        │   ├── i18nProvider.js
        │   └── initVars.js
        └── views
            └── financialMovements
            │    ├─  financialMovementsList.js
            │    └── financialMovementsShow.js   
            └── wallets
            │    ├─  walletsList.js
            │    └── walletsShow.js  
            └── walletsBalance
            │    ├─  walletsBalanceList.js
            │    └── walletsBalanceShow.js 
            └── dashboard.js
            
   
            
### public : 

Contains public codes like html, css and images.

1. wsse.js :

    Generate & return the token X-WSSE from two paramerters (username, password).
    
    wsse.js is added to public folder to be included directly in the index.html, and called from any other file by accessing to the `window` context :
    
    `const WSSEToken =  window.wsseHeader(username, password);`


### DAO : Interface with iBanFirst Api
    
1. authProvider.js : 

    Is an object that handles authentication logic. It exposes methods that react-admin calls when needed, and that return a Promise.
    
    
2. iBanFirstDataProvider.js : 


    App needs to communicate with the API, so it calls methods on the iBanFirstDataProvider Interface.
   


3. iBanFirstHttpRequest.js :
 

    Prepare the X-WSSE token after httpRequest of the current query.<br/>
    For many started requests, this script prepare two different token and return the specific httpClient for each request :
    
    
    `const iBanFirstHttpRequest = IBanFirstHttpRequest.call(); //getList() Request type `
    
    
    `const iBanFirstHttpRequest = IBanFirstHttpRequest.call();  //getOne() Request type`

    
### init : Initialisation data

1. i18n : Translation data init Config

2. i18nProvider.js :  Translation data init Manager

3. initVars.js : Load static data in session and init end points total values.


### views : 

    
1. financialMovements : List & Show Layouts for "Financial Movements" endPoint 

2. wallets :  List & Show Layouts List && Show Layouts for "Wallet" endPoint

3. walletsBalance : List & Show Layouts List && Show Layouts for "Wallet Balance" endPoint
 

    
### src :     

1. App.js : App Intro for Managing Endpoints (financialMovements, wallets and balance ) :

```
const App = () => (
    <Admin  
          dashboard={dashboard} 
          dataProvider={iBanFirstDataProvider} 
          authProvider={authProvider}
          i18nProvider={i18nProvider}
          title="iBanFirst API Admin"
    >
        <Resource name="financialMovements" list={FinancialMovementList} show={FinancialMovementShow} edit={EditGuesser} options={{ label: 'Financial Movements' }} />
        <Resource name="wallets" list={WalletsList} show={WalletsShow} edit={EditGuesser} options={{ label: 'Wallets' }} />
        <Resource name="balance" list={WalletsBalanceList} title="WalletBalance" options={{ label: 'Wallets Balance' }} />
    </Admin>
);
```

With `<Resource/>` Component is used to load data to a specific endpoint (name="specific endpoint")



## Author
[Saber Ouertani](mailto:saberouertani1985@gmail.com)



## License
Copyright (c) 2020, Saber Ouertani. 
