import { IBanFirstHttpRequest } from '../DAO/iBanFirstHttpRequest';

export const InitVars = ({ username, password }) => {

    const apiUrl = '/api';
    const endPoints = ['financialMovements','wallets'];
    let total = {
        'financialMovements':50,
        'wallets':50
    }
     
    // Balance List : Default object returned for empty filter 
    const defaultBalanceResponse = {
        "wallet": {
            "id": "XXXXXXX",
            "balance": {
                "closingDate": "yyyy-mm-dd",
                "bookingAmount": {
                    "value": "0",
                    "currency": "CUR"
                },
                "valueAmount": {
                    "value": "0",
                    "currency": "CUR"
                }
            }
        }
    };

    // Set vars in session
    localStorage.setItem('username', username);
    localStorage.setItem('password', password);
    localStorage.setItem('apiUrl', apiUrl);
    localStorage.setItem('defaultBalanceResponse', JSON.stringify(defaultBalanceResponse));
    

    // Init des Total for each endPoints
    endPoints.map(function(resource, key){
        
        if(total[resource] === 0){
            
            setTimeout(function(){ 
                // Wait 1sc after generate new Token
                let iBanFirstHttpRequest = IBanFirstHttpRequest.call();
                iBanFirstHttpRequest(`${apiUrl}/${resource}/`).then(({ headers, json }) => {
                    total[resource] = json[resource].length; 
                });

            }, 1000);
         
        }
    });


    localStorage.setItem('total', JSON.stringify(total)); 
    window.location.reload();
   
};




