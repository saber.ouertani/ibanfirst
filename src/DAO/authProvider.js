import { InitVars } from '../init/initVars';

export default {

    login: ({ username, password }) => {

        const apiUrl = '/api/financialMovements/';
        const token =  window.wsseHeader(username, password);
        const request = new Request(apiUrl, {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'X-WSSE': token }),
        });
        return fetch(request)
            .then(response => {

                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                } else {

                    // Init App Vars
                    InitVars({ username, password });

                    // Accept authentification and Redirect to Daschboard
                    //return Promise.resolve();    
                } 
            })
    },
    logout: () => {
       localStorage.clear();
       return Promise.resolve();
    },
    checkError: ({ status }) => {
       if (status === 401 || status === 403) {
           localStorage.clear();
           return Promise.reject();
       }
       return Promise.resolve();
    },
    checkAuth: () => {
       return (localStorage.getItem('username') && localStorage.getItem('password'))
           ? Promise.resolve()
           : Promise.reject();
    },
    getPermissions: () => Promise.resolve(),
};


