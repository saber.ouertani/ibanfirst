import * as React from "react";
import { Tab,TabbedShowLayout, Show, TextField, ReferenceField } from 'react-admin';

export const WalletsShow = props => (
    <Show {...props}>
        <TabbedShowLayout>
            <Tab label="Wallet Informations">
                <ReferenceField label="Wallet Id" source="id" reference="wallets" link={false}>
                    <TextField source="id" />
                </ReferenceField>
                <TextField label="Currency" source="currency" />
                <TextField label="Tag" source="tag" />
                <TextField label="Status" source="status" />
                <TextField label="Account Number" source="accountNumber" />
            </Tab>

            <Tab label="Holder Bank">
                <TextField label="Bic" source="holderBank.bic" />
                <TextField label="Clearing Code Type" source="holderBank.clearingCodeType" />
                <TextField label="Name" source="holderBank.name" />
                <TextField label="Street" source="holderBank.address.street" />
                <TextField label="Post Code" source="holderBank.address.postCode" />
                <TextField label="City" source="holderBank.address.city" />
                <TextField label="Province" source="holderBank.address.province" />
                <TextField label="Country" source="holderBank.address.country" />
            </Tab>

            <Tab label="Holder">  
                <TextField label="Name" source="holder.name" />
                <TextField label="Type" source="holder.type" />
                <TextField label="Street" source="holder.address.street" />
                <TextField label="Post Code" source="holder.address.postCode" />
                <TextField label="City" source="holder.address.city" />
                <TextField label="Province" source="holder.address.province" />
                <TextField label="Country" source="holder.address.country" />
            </Tab>
        </TabbedShowLayout>
    </Show>
);