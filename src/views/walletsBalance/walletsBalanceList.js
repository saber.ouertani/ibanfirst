import * as React from "react";
import { DateInput, AutocompleteInput, ReferenceInput, Filter, List, Datagrid, TextField } from 'react-admin';
import { WalletsBalanceShow } from './walletsBalanceShow';

const WalletsBalanceIdFilter = (props) => (
    <Filter {...props}>
        <ReferenceInput source="walletId" reference="wallets" alwaysOn>
            <AutocompleteInput optionText="id" />
        </ReferenceInput>
        <DateInput source="bookingDate" alwaysOn/>
    </Filter>
);
 
export const WalletsBalanceList = props => (
    <List {...props} title="Wallets List" filters={<WalletsBalanceIdFilter/>} >
        <Datagrid expand={<WalletsBalanceShow />}>
            <TextField label="Wallet Id" source="id" />
            <TextField label="Closing Date" source="balance.closingDate" />
            <TextField label="Booking Amount" source="balance.bookingAmount.value" />
            <TextField label="Booking Currency" source="balance.bookingAmount.currency" />
            <TextField label="Value Amount" source="balance.valueAmount.value" />
            <TextField label="Value Currency" source="balance.valueAmount.currency" />
        </Datagrid>
    </List>
);