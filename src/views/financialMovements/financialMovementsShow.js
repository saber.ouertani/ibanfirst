import * as React from "react";
import { Tab, TabbedShowLayout, Show, DateField, TextField, ReferenceField } from 'react-admin';

export const FinancialMovementShow = (props) => (
    <Show {...props}>
        <TabbedShowLayout>
           <Tab label="Booking Informations">
            <ReferenceField label="Movement Id" source="id" reference="financialMovements" link={false}><TextField source="id" /></ReferenceField>
            <DateField label="Booking Date" source="bookingDate" />
            <DateField label="Movement Date" source="valueDate" />
            <TextField label="Movement Amount" source="orderingAmount.value" />
            <TextField label="Description" source="description" />
            </Tab>
            <Tab label="Ordering Informations">
                <ReferenceField source="walletId" reference="wallets" link="show"><TextField source="id" /></ReferenceField>
                <TextField label="Ordering Account Number" source="orderingAccountNumber" />
                <TextField label="Ordering Customer" source="orderingCustomer" />
                <TextField label="Ordering Institution" source="orderingInstitution" />
                <TextField label="Ordering Amount" source="orderingAmount.value" />
                <TextField label="Ordering Currency" source="orderingAmount.currency" />

            </Tab>
            <Tab label="beneficiary Informations">
                <TextField label="Beneficiary Account Number" source="beneficiaryAccountNumber" />
                <TextField label="Beneficiary Customer" source="beneficiaryCustomer" />
                <TextField label="Beneficiary Institution" source="beneficiaryInstitution" />
                <TextField label="Beneficiary Amount" source="beneficiaryAmount.value" />
                <TextField label="Beneficiary Currency" source="beneficiaryAmount.currency" />
            </Tab>
            <Tab label="Others">
                <TextField label="Remittance Information" source="remittanceInformation" />
                <TextField label="Charges Details" source="chargesDetails" />
                <TextField label="Exchange Rate" source="exchangeRate" />
                <TextField label="Type Label" source="typeLabel" />
                <TextField label="Internal Reference" source="internalReference" />
            </Tab>
        </TabbedShowLayout>
    </Show>
);
