import * as React from "react";
import { Tab,TabbedShowLayout, Show, TextField, ReferenceField } from 'react-admin';

export const WalletsBalanceShow = props => (
    <Show {...props} title="Wallets Show">
        <TabbedShowLayout>
            <Tab label="Balance Details">
                <TextField label="Closing Date" source="balance.closingDate" />
                <TextField label="Booking Amount" source="balance.bookingAmount.value" />
                <TextField label="Booking Currency" source="balance.bookingAmount.currency" />
                <TextField label="Value Amount" source="balance.valueAmount.value" />
                <TextField label="Value Currency" source="balance.valueAmount.currency" />
             </Tab>
            <Tab label="Wallet">
                <ReferenceField label="Go to Wallet" source="id" reference="wallets" link="show">
                    <TextField source="id" />
                </ReferenceField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);