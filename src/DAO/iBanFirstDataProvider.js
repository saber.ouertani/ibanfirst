import { stringify } from 'query-string';
import { IBanFirstHttpRequest } from './iBanFirstHttpRequest';

// Init common const for differents request method
const apiUrl = localStorage.getItem('apiUrl');
const defaultBalanceResponse = JSON.parse(localStorage.getItem('defaultBalanceResponse'));
const total = JSON.parse(localStorage.getItem('total'));

export default {
    /** Called for List Views **/
    getList: (resource, params) => {
        // Prepare HttpRequest with new X-WSSE Token
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();

        // Listing wallets balance 
        if(resource === 'balance'){
            if(params.filter.walletId !== undefined && params.filter.bookingDate!== undefined){
                
                const walletId = params.filter.walletId;
                const bookingDate = params.filter.bookingDate;
                resource = 'wallets';

                const url = `${apiUrl}/${resource}/-${walletId}/balance/${bookingDate}`;
                return iBanFirstHttpRequest(url).then(({ headers, json }) => ({
                    data: [json['wallet']],
                    total: 1,
                }));

            } else {
                return Promise.resolve(true).then(({ headers, json }) => ({
                    data: [defaultBalanceResponse['wallet']],
                    total: 1,
                })); 
            } 
           
        } else { // Default Listing  

            const { page, perPage } = params.pagination;
            const { sort } = params.sort;
            const query = {
                sort: JSON.stringify(sort),
                page: JSON.stringify(page),
                per_page: JSON.stringify(perPage),
            };

            const url = `${apiUrl}/${resource}/?${stringify(query)}`; 
            return iBanFirstHttpRequest(url).then(({ headers, json }) => ({
                data: json[resource],
                total: (total[resource] ? total[resource] : 50)
            }));

        }
  
    },

    /** Called for Show Views **/
    getOne: (resource, params) => {

        if(params.id === 'XXXXXXX'){
            return Promise.resolve(true).then(({ headers, json }) => ({
                data: [defaultBalanceResponse['wallet']],
                total: 1,
            }));  
        } else { 
            resource = (resource === 'balance') ? 'wallets' : resource;
            const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
            return iBanFirstHttpRequest(`${apiUrl}/${resource}/-${params.id}`).then(({ json }) => ({
                data: [json[resource]],
                total: 1,
            })); 
        } 
 
    }, 
    /** 
        Called for Linked Json Objects : Mapping One To One 
        Example : financialMovements.wallet_id <=> wallets.id) 
    **/ 
    getMany: (resource, params) => {
        // Manage returned object name (endPoint)
        const endPoint = (resource === 'wallets') ? 'wallet' : 'financialMovement';
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();

        return iBanFirstHttpRequest(`${apiUrl}/${resource}/-${params.ids[0]}`).then(({ json }) => ({
            data: [json[endPoint]]
        }));
        
    },
    /** Called for Link Json Objects : One To Many, Not supported for test access **/
    getManyReference: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;

        return iBanFirstHttpRequest(url).then(({ headers, json }) => ({
            data: json,
            total: parseInt(headers.get('content-range').split('/').pop(), 10),
        }));
    },

    /** Called for one element update action : Not supported for test access **/
    update: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        return iBanFirstHttpRequest(`${apiUrl}/${resource}/${params.id}`, {
            method: 'PUT',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }))
    },

    /** Called for massive update action : Not supported for test access **/
    updateMany: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        return iBanFirstHttpRequest(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'PUT',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }));
    },

    /** Called for create action : Not supported for test access **/
    create: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        return iBanFirstHttpRequest(`${apiUrl}/${resource}`, {
            method: 'POST',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({
            data: { ...params.data, id: json.id },
        }))
    },

    /** Called for delete action : Not supported for test access **/
    delete: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        return iBanFirstHttpRequest(`${apiUrl}/${resource}/${params.id}`, {
            method: 'DELETE',
        }).then(({ json }) => ({ data: json }))
    },

    /** Called for massive delete action : Not supported for test access **/
    deleteMany: (resource, params) => {
        const iBanFirstHttpRequest = IBanFirstHttpRequest.call();
        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };
        return iBanFirstHttpRequest(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'DELETE',
            body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json }));
    }
};
