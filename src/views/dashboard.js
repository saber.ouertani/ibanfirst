import * as React from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Title } from 'react-admin';
import YouTube from 'react-youtube';

const opts = {
      height: '490',
      width: '100%',
      playerVars: {
        autoplay: 0,
      },
    };

export default () => (
    <Card>
        <Title title="iBanFirst API Admin" />
        <CardContent>
	        <YouTube videoId="6wh3APhM8WA" opts={opts}  />
        </CardContent>
    </Card>
);