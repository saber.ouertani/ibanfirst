import * as React from "react";
import { List, Datagrid, DateField, TextField } from 'react-admin';
 
export const WalletsList = props => ( 
    <List {...props} title="Wallets List">
        <Datagrid rowClick="show">
            <TextField label="Wallet Id" source="id" />
            <TextField label="Tag" source="tag" />
            <TextField label="Currency" source="currency" />
            <TextField label="Booking Amount" source="bookingAmount.value" />
            <TextField label="Value Amount" source="valueAmount.value" />
            <DateField label="Last Movement" source="dateLastFinancialMovement" />
        </Datagrid>
    </List>
);
