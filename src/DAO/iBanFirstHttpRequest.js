import { fetchUtils } from 'react-admin';


export const IBanFirstHttpRequest = () => {

    const username = localStorage.getItem('username');
    const password = localStorage.getItem('password');
    const WSSEToken =  window.wsseHeader(username, password);
           
    const httpClient = (url, options = {}) => {
        if (!options.headers) {
            options.headers = new Headers({ 
               Accept: 'application/json',
               'X-WSSE': WSSEToken
            });
        }
        return fetchUtils.fetchJson(url, options);
    };

    return httpClient;
};