import * as React from "react";
import { List, Datagrid, DateField, TextField } from 'react-admin';

export const FinancialMovementList = props => (
    <List {...props} title="Financial Movements List">
        <Datagrid rowClick="show">
            <TextField label="Movement Id" source="id" />
            <DateField source="bookingDate" />
            <DateField source="valueDate" />
            <TextField source="walletId" />
            <TextField source="amount.value" />
            <TextField source="description" />
        </Datagrid>
    </List>
);