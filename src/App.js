// React js project
// in src/App.js 
import * as React from "react";
import { Admin, Resource, EditGuesser } from 'react-admin';
import iBanFirstDataProvider from './DAO/iBanFirstDataProvider';
import dashboard from './views/dashboard';
import { FinancialMovementList } from './views/financialMovements/financialMovementsList';
import { FinancialMovementShow } from './views/financialMovements/financialMovementsShow';
import { WalletsList } from './views/wallets/walletsList';
import { WalletsShow } from './views/wallets/walletsShow';
import { WalletsBalanceList } from './views/walletsBalance/walletsBalanceList';
import authProvider from './DAO/authProvider';
import i18nProvider from './init/i18nProvider';


 
const App = () => (
    <Admin  
          dashboard={dashboard} 
          dataProvider={iBanFirstDataProvider} 
          authProvider={authProvider}
          i18nProvider={i18nProvider}
          title="iBanFirst API Admin"
    >
        <Resource name="financialMovements" list={FinancialMovementList} show={FinancialMovementShow} edit={EditGuesser} options={{ label: 'Financial Movements' }} />
        <Resource name="wallets" list={WalletsList} show={WalletsShow} edit={EditGuesser} options={{ label: 'Wallets' }} />
        <Resource name="balance" list={WalletsBalanceList} title="WalletBalance" options={{ label: 'Wallets Balance' }} />
    </Admin>
);
export default App;
